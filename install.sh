#!/bin/bash
cd Source
make
cd ..
if [ -e /usr/lib/avrduino/ ]; then
  if [ ! -e /usr/bin/avrduino ]; then
    echo "Armed file from /usr/bin is missing. Reinstalling . . ."
    sudo cp -r Source/armed /usr/bin/
  else
  echo "Armed is already installed."
  fi  
else
  sudo mkdir /usr/lib/armed
  sudo cp -r Source/data/ /usr/lib/armed
  sudo cp -r Source/docs/ /usr/lib/armed
  sudo cp -r Source/script/ /usr/lib/armed
  sudo cp -r Source/unistall.sh /usr/lib/armed
  if [ -e /usr/bin/armed ]; then
    rm -r /usr/bin/armed
  fi 
  sudo cp Source/armed /usr/bin/
  echo "Armed successfully installed. "
fi
  sudo cp Source/autocomplete/armed /etc/bash_completion.d/

