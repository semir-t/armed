#!/bin/bash

BLACK='\033[0;30m'
RED='\033[0;31m'
GREEN='\033[0;32m'
BROWN='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
LGRAY='\033[0;37m'
DGRAY='\033[1;30m'
LRED='\033[1;31m'
LGREEN='\033[1;32m'
YELLOW='\033[1;33m'
LBLUE='\033[1;34m'
LPURPLE='\033[1;35m'
LCYAN='\033[1;36m'
WHITE='\033[1;37m'
NC='\033[0m'

makefile()
{
  BOARD="default"
  PS3='Chose your board: '
  options=("uno" "mega" "mega2560" "atmega8" "atmega168" "atmega328" "pro" "pro5v" "pro328" "pro5v328")
  select opt in "${options[@]}"
  do case $opt in 
    "uno") BOARD="uno"
    cp -r /usr/lib/armed/data/boards_info/uno/board-info.h .
    MCU="atmega328p"
    F_CPU="16000000UL"
    ;;
  "mega") BOARD="mega" 
    cp -r /usr/lib/armed/data/boards_info/mega/board-info.h .
    MCU="atmega2560"
    F_CPU="16000000UL"
    ;;
  "mega2560") BOARD="mega2560"
    cp -r /usr/lib/armed/data/boards_info/mega2560/board-info.h .
    MCU="atmega2560"
    F_CPU="16000000UL"
    ;;
  "atmega8") BOARD="atmega8"
    cp -r /usr/lib/armed/data/boards_info/atmega8/board_-nfo.h .
    MCU="atmega8"
    F_CPU="16000000UL"
    ;;
  "atmega168") BOARD="atmega168"
    cp -r /usr/lib/armed/data/boards_info/atmega168/board-info.h .
    MCU="atmega168"
    F_CPU="16000000UL"
    ;;
  "atmega328") BOARD="atmega328"
    cp -r /usr/lib/armed/data/boards_info/atmega328/board-info.h .
    MCU="atmega328p"
    F_CPU="16000000UL"
    ;;
  "pro") BOARD="pro"
    cp -r /usr/lib/armed/data/boards_info/pro/board-info.h .
    MCU="unknow"
    F_CPU="16000000UL"
    ;;
  "pro5v") BOARD="pro5v"
    cp -r /usr/lib/armed/data/boards_info/pro5v/board-info.h .
    MCU="unknown"
    F_CPU="16000000UL"
    ;;
    
  "pro328") BOARD="pro328"
    cp -r /usr/lib/armed/data/boards_info/pro328/board-info.h .
    MCU="atmega328p"
    F_CPU="16000000UL"
    ;;
  "pro5v328") BOARD= "pro5v328"
    cp -r /usr/lib/armed/data/boards_info/pro5v328/board-info.h .
    MCU="atmega328p"
    F_CPU="16000000UL"
    ;;
  *)
    echo "Error : Input is not valid"
    echo "Exiting..."
    return 1 
    ;;
    esac
    break
  done
  [ -e Makefile ] && rm Makefile
  read -p "Do you want to configure your Makefile settings [Y/n]: " CONFIGURE
  if { [ "$CONFIGURE" == "Y" ] || [ "$CONFIGURE" == "y" ]; }; then
    read -p "Enter your MCU: " MCU
    read -p "Enter F_CPU: " F_CPU
  fi
  read -p "Enter ARDUINO_PORT: " ARDUINO_PORT
  echo "ARDUINO_DIR = /usr/share/arduino">>Makefile
  echo "BOARD_TAG = $BOARD">>Makefile
  echo "ARDUINO_PORT = $ARDUINO_PORT">>Makefile
  echo "NO_CORE = 1">>Makefile
  echo "AVRDUDE_ARD_PROGRAMMER = arduino">>Makefile
  echo "HEX_MAXIMUM_SIZE = 30720">>Makefile
  echo "AVRDUDE_ARD_BAUDRATE = 115200">>Makefile
  echo "#ISP_LOW_FUSE = 0xFF">>Makefile
  echo "#ISP_HIGH_FUSE = 0xDA">>Makefile
  echo "#ISP_EXT_FUSE = 0x05">>Makefile
  echo "#ISP_LOCK_FUSE_PRE = 0x3F">>Makefile
  echo "#ISP_LOCK_FUSE_POST = 0x0F">>Makefile
  echo "MCU = $MCU">>Makefile
  echo "F_CPU = $F_CPU">>Makefile
  echo "VARIANT = standard">>Makefile
  echo "ARDUINO_LIBS =">>Makefile
  echo "include /usr/share/arduino/Arduino.mk">>Makefile
  echo "$BOARD|$MCU|" >> .armed.txt

  clear
  echo -e "${LGREEN}Makefile settings${NC}"
  echo -e "${LBLUE}ARDUINO_DIR = ${LRED}/usr/share/arduino ${NC}"
  echo -e "${LBLUE}BOARD_TAG = ${LRED}$BOARD${NC}"
  echo -e "${LBLUE}ARDUINO_PORT = ${LRED}$ARDUINO_PORT${NC}"
  echo -e "${LBLUE}NO_CORE = ${LRED}1${NC}"
  echo -e "${LBLUE}AVRDUDE_ARD_PROGRAMMER = ${LRED}arduino${NC}"
  echo -e "${LBLUE}HEX_MAXIMUM_SIZE = ${LRED}30720${NC}"
  echo -e "${LBLUE}AVRDUDE_ARD_BAUDRATE = ${LRED}115200${NC}"
  echo -e "${DGRAY}#ISP_LOW_FUSE = ${RED}0xFF${NC}"
  echo -e "${DGRAY}#ISP_HIGH_FUSE = ${RED}0xDA${NC}"
  echo -e "${DGRAY}#ISP_EXT_FUSE = ${RED}0x05${NC}"
  echo -e "${DGRAY}#ISP_LOCK_FUSE_PRE = ${RED}0x3F${NC}"
  echo -e "${DGRAY}#ISP_LOCK_FUSE_POST = ${RED}0x0F${NC}"
  echo -e "${LBLUE}MCU = ${LRED}$MCU${NC}"
  echo -e "${LBLUE}F_CPU = ${LRED}$F_CPU${NC}"
  echo -e "${LBLUE}VARIANT = ${LRED}standard${NC}"
  echo -e "${LBLUE}ARDUINO_LIBS =${NC}"

}

initializeProject()
{
  read -p "Project name: " PROJECT_NAME
  if [ ! -d $PROJECT_NAME ]; then
    mkdir $PROJECT_NAME
    cd $PROJECT_NAME
    makefile #Call function that makes makefile
    cp -r /usr/lib/armed/data/include/ .
    echo -e "${LGREEN}Project created successfully ${NC}"
  else 
    echo "Armed: Project with name [ $PROJECT_NAME ] already exists. "
    echo "Armed: Stop project wizard and exit."
  fi
}

clear
initializeProject

