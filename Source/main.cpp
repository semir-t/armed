/********************************************************************************************
 * Armed 
 * version: 1.00
 *
 *
 * Last modified: 09.10.2016
********************************************************************************************/
#include <iostream>
#include <string>
#include "include/Commands.hxx"

#define programName 0
#define firstCommand 1

int main(int argc, char *argv[])
{
 if(argc == 1) 
 {
   initializeProject(); // run bash script that creates files used for project
 }
 else 
 {
    if(std::string(argv[firstCommand]) == "datasheet") 
    {
      datasheet(); // run datasheet for used microcontroller
    }
    else if( std::string(argv[firstCommand]) == "pinout" )
    {
      pinout(); // show used board pinout as image
    }
    else if( std::string(argv[firstCommand]) == "boardinfo" )
    {
      boardInfo(); //show info for used board 
    }
    else if( std::string(argv[firstCommand]) == "--include" )
    {
      include(argc,argv); // update used defined  include librery with desired files
    }
    else if( std::string(argv[firstCommand]) == "--help" )
    {
      help();// display help
    }
    else
    {
      std::cout << std::endl;
      std::cout << "Armed: Command [" << std::string(argv[firstCommand]) << "] is not valid." << std::endl;
      std::cout << "See 'armed --help' for more info." << std::endl;
      std::cout << std::endl;
    }
 }
 return 0;
}
