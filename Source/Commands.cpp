#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>
#include "include/Commands.hxx"

bool fileExists(const std::string &);
bool readFile(std::string &, std::string &);

//Run initialize script that creates neccesery files
void initializeProject()
{
  system("/usr/lib/armed/script/initialize.sh");
  return;
}

//Determine which MCU is used by reading the project file and run specific datasheet
void datasheet()
{
  std::string board, mcu;
  if(readFile(board,mcu))
  {
    if(mcu == "atmega328p" || mcu == "atmega168" || mcu == "atmega168PA" || mcu == "atmega168A" || mcu == "atmega88A" || mcu == "atmega88PA")
    {
      std::cout << "Armed: Run datasheet for " << mcu << std::endl;
      system("gnome-open /usr/lib/armed/data/datasheet/atmega168-328P.pdf");
      std::cout << "Armed: Done" << std::endl;
    }
    else if( mcu == "atmega2561" || mcu == "atmega2561V" || mcu == "atmega2560" || mcu == "atmega2560V" || mcu == "atmega1281" || mcu == "atmega1281V" || mcu == "atmega1280" || mcu == "atmega1280V")
    {
      std::cout << "Armed: Run datasheet for " << mcu << std::endl;
      system("gnome-open /usr/lib/armed/data/datasheet/atmega2560.pdf");
      std::cout << "Armed: Done" << std::endl;
    }
    else if(mcu == "atmega8" || mcu == "atmega8L")
    {
      std::cout << "Armed: Run datasheet for " << mcu << std::endl;
      system("gnome-open /usr/lib/armed/data/datasheet/atmega8.pdf");
      std::cout << "Armed: Done" << std::endl;
    }
    else
    {
      std::cout << "Armed: No datasheet for mcu [" << mcu << "]";
    }
  }
  return;
}

//Determine which board is used by reading the project file and run specific pinout image 
void pinout()
{
  std::string board, mcu;
  if(readFile(board,mcu))
  {
    if(board == "uno")
    {
      std::cout << "Armed: Run pinout image for uno" << std::endl;;
      system("gnome-open /usr/lib/armed/data/boards_info/uno/uno.png");
      std::cout << "Armed: Done" << std::endl;
    }
    else if(board == "mega")
    {
      std::cout << "Armed: Run pinout image for mega" << std::endl;;
      system("gnome-open /usr/lib/armed/data/boards_info/mega/mega.png");
      std::cout << "Armed: Done" << std::endl;
    }
    else if(board == "mega2560")
    {
      std::cout << "Armed: Run pinout image for mega2560" << std::endl;;
      system("gnome-open /usr/lib/armed/data/boards_info/mega2560/mega2560.png");
      std::cout << "Armed: Done" << std::endl;
    }
    else if(board == "atmega8")
    {
      std::cout << "Armed: Run pinout image for atmega8" << std::endl;;
      system("gnome-open /usr/lib/armed/data/boards_info/atmega8/atmega8.jpg");
      std::cout << "Armed: Done" << std::endl;
    }
    else if(board == "atmega168")
    {
      std::cout << "Armed: Run pinout image for atmega168" << std::endl;;
      system("gnome-open /usr/lib/armed/data/boards_info/atmega168/atmega168.png");
      std::cout << "Armed: Done" << std::endl;
    }
    else if(board == "atmega328")
    {
      std::cout << "Armed: Run pinout image for atmega328" << std::endl;;
      system("gnome-open /usr/lib/armed/data/boards_info/atmega328/atmega328.png");
      std::cout << "Armed: Done" << std::endl;
    }
    else
    {
      std::cout << "Armed: No pinout scheme for board[" << board <<"]" << std::endl;
    }
  }  
  return;
}



//Print help to user
void help()
{
  std::cout << "Usage: armed [command] <file> ..." << std::endl;
  std::cout << std::endl;
  std::cout << "To create new project run 'armed' and follow instruction. This will create all neccesery files that will be later used by program." << std::endl;
  std::cout << "Every project contains:\n\t Makefile \n\t include file that has users libraries\n\t hidden file '.armed.txt'" << std::endl;
  std::cout << "File '.armed.txt' SHOULD NOT be erased." << std::endl;
  std::cout << "Armed commands: \n"  << std::endl;
  std::cout << "\t datasheet: \t run the datasheet for the MCU used in this project," << std::endl;
  std::cout << "\t pinout: \t run the pinout scheme for the board used in this project." << std::endl;
  std::cout << "\t boardinfo: \t for boards UNO and MEGA there is more detail description that you can see by running this command." << std::endl;
  std::cout << "\t --include: \t If there is some header file that you want to include in every project then run this command" << std::endl;
  std::cout << "\t \t \t example: 'armed include delay.h', now every project that we create will have folder include with 'delayh.h' in it" << std::endl << std::endl;;
  std::cout << "To compile project run 'make'." << std::endl;
  std::cout << std::endl;
  std::cout << "To unistall Armed run script located in /usr/lib/armed/ " << std::endl;
  std::cout << std::endl;
  return;
}

//show info for used board
void boardInfo()
{
  std::string board, mcu ;
  if(readFile(board,mcu))
  {
    if(board == "uno")
    {
      std::cout << "Armed: Run info for UNO board" << std::endl;;
      system("gnome-open /usr/lib/armed/data/boards_info/uno/info.pdf");
      std::cout << "Armed: Done" << std::endl;

    }
    else if (board == "mega" || board == "mega2560")
    {
      std::cout << "Armed: Run info for MEGA board" << std::endl;;
      system("gnome-open /usr/lib/armed/data/boards_info/mega/info.pdf");
      std::cout << "Armed: Done" << std::endl;
    }
    else
    {
      std::cout << "Armed: Info for the given board is not available. " << std::endl;
    }
  }
  return;
}

//Copy files to used defined library
void include(int argc, char *argv[])
{
  for(int i = 2; i != argc; ++i)  
  {
    std::string command = std::string("cp ") + std::string(argv[i]) + std::string(" /usr/lib/avdruino/data/include/");
    system(command.c_str());
  }
}

//Check if file with the given name exists
bool fileExists(const std::string & fileName)
{
  std::ifstream inputFile(fileName);
  return (bool) inputFile;
}

//Read file and change values of board and mcu according to data read from the file
bool readFile(std::string & board, std::string & mcu)
{
  if(fileExists(".armed.txt"))  
  {
    std::ifstream inputFile(".armed.txt");
    if(inputFile.is_open())
    {
      getline(inputFile,board,'|');
      getline(inputFile,mcu,'|');
      return true ;
    }
    else
    {
      std::cout << "Armed: Problem occured while opening file '.armed.txt'" << std::endl;
      return false;
    }
  }
  else
  {
    std::cout << std::endl;
    std::cout << "Armed: Project file '.armed.txt' is missing " << std::endl;
    std::cout << "To use Armed you must create new project" << std::endl;
    std::cout << "See 'armed --help' for more info." << std::endl;
    std::cout << std::endl;
    return false;
  }
}
