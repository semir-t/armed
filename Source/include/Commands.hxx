#ifndef _COMMANDS_HXX 
#define _COMMANDS_HXX

void initializeProject();
void datasheet();
void pinout();
void help();
void boardInfo();
void include(int argc, char *argv[]);

#endif /* ifndef _COMMANDS_HXX */
