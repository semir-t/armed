# Armed

Armed helps user to  quickly and easily create projects, acces the datasheet and pinout scheme for used MCU or to documentation for used board (Arduino UNO, MEGA etc.)

For every project Makefile is created, and all user-created libraries are copied to include folder located in the project folder.

## Installation

Ubuntu:

```sh
./install.sh
. /etc/bash_completion.d/armed
```

## Usage example

Usage: armed [command] <file> ...

To create new project run 'armed' and follow instruction. This will create all neccesery files that will be later used by program.
Every project contains:

* Makefile,  

* include file that has users libraries,

* hidden file '.armed.txt'.

File '.armed.txt' SHOULD NOT be erased.

Armed commands:

* datasheet:    run the datasheet for the MCU used in this project,

* pinout:       run the pinout scheme for the board used in this project.

* boardinfo:    for boards UNO and MEGA there is more detail description that you can see by running this command.

* --include:    If there is some header file that you want to include in every project then run this command.
[example: 'armed include delay.h', now every project that we create will have folder include with 'delayh.h' in it]

To compile project run 'make'.
To unistall Armed run script located in /usr/lib/armed/

## Release History

* 1.0
    * The first proper release

## Meta

Semir Tursunovic – tursunovic.semir@gmail.com

Distributed under the GNU license. See ``LICENSE`` for more information.

[https://bitbucket.org/semir_t/]

[npm-image]: https://img.shields.io/npm/v/datadog-metrics.svg?style=flat-square
[npm-url]: https://npmjs.org/package/datadog-metrics
[npm-downloads]: https://img.shields.io/npm/dm/datadog-metrics.svg?style=flat-square
[travis-image]: https://img.shields.io/travis/dbader/node-datadog-metrics/master.svg?style=flat-square
[travis-url]: https://travis-ci.org/dbader/node-datadog-metrics